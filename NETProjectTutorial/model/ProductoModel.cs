﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> productos = new List<Producto>();
        private DaoImplementsProducto product;


        public ProductoModel()
        {
            product = new DaoImplementsProducto();
        }
        public  List<Producto> GetProductos()
        {
            return product.findAll();
        }

        public void Populate()
        {
            /*Producto[] pdts =
            {
                new Producto(1,"Milk2514","Leche entera","Leche enterea 3% grasa",25,30.0),
                new Producto(2,"Milk2515","Leche descremada","Leche descremada 0% grasa",25,40.0),
                new Producto(3,"Milk2516","Leche deslactosada","Leche deslactosada 2% grasa",25,35.0)
            };*/

           productos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach(Producto p in productos)
            {
                product.save(p);
            }
        }
        public void save(DataRow producto)
        {
            Producto p = new Producto();
            p.Id= Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripcion"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());

            product.save(p);
        }

        internal void update(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripcion"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());

            product.update(p);
        }
    }
}
