﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Producto
    {
        private int id;//4
        private string sku;//9 * 2 => 18+3=21
        private string nombre;//40=>83
        private string descripcion;//100=>203
        private int cantidad;//4
        private double precio;//8
        //Total => 323+0

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public Producto(int id, string sku, string nombre, string descripcion, int cantidad, double precio)
        {
            this.Id = id;
            this.Sku = sku;
            this.Nombre = nombre;
            this.Descripcion = descripcion;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

        public Producto()
        {
        }

        public override string ToString()
        {
            return Sku + " " + Nombre ;
        }
    }
}

