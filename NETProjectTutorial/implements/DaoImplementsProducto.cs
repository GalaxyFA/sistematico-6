﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.dao;
using NETProjectTutorial.entities;


namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : IDaoProductos
    {
        //Header Producto
        private BinaryReader brHProducto;
        private BinaryWriter bwHProducto;
        //Datos Producto
        private BinaryReader brDProducto;
        private BinaryWriter bwDProducto;
        private FileStream fHCliente;
        private FileStream fDCliente;
        private const string PRODUCTO_HEADER = "hproducto.dat";
        private const string PRODUCTO_DATA = "dproductos.dat";
        private const int size = 323;

        public DaoImplementsProducto() { }

        private void open()
        {
            try
            {
                fDCliente = new FileStream(PRODUCTO_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(PRODUCTO_HEADER))
                {
                    fHCliente = new FileStream(PRODUCTO_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brHProducto = new BinaryReader(fHCliente);
                    bwHProducto = new BinaryWriter(fHCliente);

                    brDProducto = new BinaryReader(fDCliente);
                    bwDProducto = new BinaryWriter(fDCliente);

                    bwHProducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwHProducto.Write(0);
                    bwHProducto.Write(0);
                }
                else
                {
                    fHCliente = new FileStream(PRODUCTO_HEADER,
                       FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brHProducto = new BinaryReader(fHCliente);
                    bwHProducto = new BinaryWriter(fHCliente);
                    brDProducto = new BinaryReader(fDCliente);
                    bwDProducto = new BinaryWriter(fDCliente);
                }
            }catch(IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        public void close()
        {
            try
            {
                if(brDProducto != null)
                {
                    brDProducto.Close();
                }
                if(brHProducto != null)
                {
                    brHProducto.Close();
                }
                if(bwDProducto != null)
                {
                    bwDProducto.Close();
                }
                if(bwHProducto != null)
                {
                    bwHProducto.Close();
                }
                if(fDCliente != null){
                    fDCliente.Close();
                }
                if(fHCliente != null)
                {
                    fHCliente.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> productos = new List<Producto>();

            brHProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHProducto.ReadInt32();
            
            for(int i =0; i<n; i++)
            {
                long hpos = 8 + i + 4;
                brDProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brHProducto.ReadInt32();

                long dpos = (index - 1) * size;
                brDProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brDProducto.ReadInt32();
                string sku = brDProducto.ReadString();
                string nombre = brDProducto.ReadString();
                string descripcion = brDProducto.ReadString();
                int cantidad = brDProducto.ReadInt32();
                double precio = brDProducto.ReadDouble();
                Producto p = new Producto(id, sku, nombre, descripcion,
                    cantidad, precio);
                productos.Add(p);
            }
            close();
            return productos;
        }

        public Producto findbyId(int id)
        {
            throw new NotImplementedException();
        }

        public Producto findBySKU(string SKU)
        {
            throw new NotImplementedException();
        }

        public void save(Producto t)
        {
            open();
            brHProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHProducto.ReadInt32();
            int k = brHProducto.ReadInt32();

            long dpos = k * size;
            bwDProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwDProducto.Write(++k);
            bwDProducto.Write(t.Sku);
            bwDProducto.Write(t.Nombre);
            bwDProducto.Write(t.Descripcion);
            bwDProducto.Write(t.Cantidad);
            bwDProducto.Write(t.Precio);

            bwHProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwHProducto.Write(++n);
            bwHProducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwHProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwHProducto.Write(k);
            close();
        }

        public int update(Producto t)
        {
            open();
            brHProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            long dpos = (t.Id - 1) * size;
            bwDProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwDProducto.Write(t.Id);
            bwDProducto.Write(t.Sku);
            bwDProducto.Write(t.Nombre);
            bwDProducto.Write(t.Descripcion);
            bwDProducto.Write(t.Cantidad);
            bwDProducto.Write(t.Precio);

            close();
            return t.Id;
        }
    }
}
