﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NETProjectTutorial.entities.Empleado;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : IDaoEmpleado
    {
        //header cliente
        private BinaryReader brHEmpleado;
        private BinaryWriter bwHEmpleado;
        //data cliente
        private BinaryReader brDEmpleado;
        private BinaryWriter bwDEmpleado;
        private FileStream HEmpleado;
        private FileStream DEmpleado;
      
        private const string FILENAME_HEADER = "hempleado.dat";
        private const string FILENAME_DATA = "dempleado.dat";
        private const int SIZE = 397;

        private void open()
        {
            try
            {
                DEmpleado = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                   HEmpleado= new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brHEmpleado  = new BinaryReader(HEmpleado);
                    bwHEmpleado = new BinaryWriter(HEmpleado);

                    brDEmpleado   = new BinaryReader(DEmpleado);
                    bwDEmpleado = new BinaryWriter(DEmpleado);

                    bwHEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwHEmpleado.Write(0);//n
                    bwHEmpleado.Write(0);//k
                }
                else
                {
                    HEmpleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brHEmpleado = new BinaryReader(HEmpleado);
                    bwHEmpleado = new BinaryWriter(HEmpleado);
                    brDEmpleado = new BinaryReader(DEmpleado);
                    bwDEmpleado = new BinaryWriter(DEmpleado);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        public void close()
        {
            try
            {
                if(brDEmpleado != null)
                {
                    brDEmpleado.Close();
                }
                if(brHEmpleado != null)
                {
                    brHEmpleado.Close();
                }
                if(bwDEmpleado != null)
                {
                    bwDEmpleado.Close();
                }
                if(bwHEmpleado != null)
                {
                    bwHEmpleado.Close();
                }
                if(DEmpleado != null)
                {
                    DEmpleado.Close();
                }
                if(HEmpleado != null)
                {
                    HEmpleado.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> empleados = new List<Empleado>();

            brHEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHEmpleado.ReadInt32();
            for(int i =0; i<n; i++)
            {
                long hpos = 8 + i * 4;
                brHEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brHEmpleado.ReadInt32();

                long dpos = (index - 1) * SIZE;
                brDEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brDEmpleado.ReadInt32();
                string nombre = brDEmpleado.ReadString();
                string apellidos = brDEmpleado.ReadString();
                string cedula = brDEmpleado.ReadString();
                string inss = brDEmpleado.ReadString();
                string direccion = brDEmpleado.ReadString();
                double salario = brDEmpleado.ReadDouble();
                string tconvencional = brDEmpleado.ReadString();
                string tcelular = brDEmpleado.ReadString();
                int sexo = brDEmpleado.ReadInt32();
                Empleado e = new Empleado(id, nombre, apellidos, cedula, inss, direccion, salario
                    , tconvencional, tcelular, (SEXO)Enum.ToObject(typeof(SEXO),sexo));
                empleados.Add(e);
            }
            close();
            return empleados;
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            open();
            brHEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHEmpleado.ReadInt32();
            int k = brHEmpleado.ReadInt32();

            long dpos = k * SIZE;
            bwDEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwDEmpleado.Write(++k);
            bwDEmpleado.Write(t.Nombre);
            bwDEmpleado.Write(t.Apellidos);
            bwDEmpleado.Write(t.Cedula);
            bwDEmpleado.Write(t.Inss);
            bwDEmpleado.Write(t.Direccion);
            bwDEmpleado.Write(t.Salario);
            bwDEmpleado.Write(t.Tconvencional);
            bwDEmpleado.Write(t.Tcelular);
            //bwDEmpleado.Write();

            bwHEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            bwHEmpleado.Write(++n);
            bwHEmpleado.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwHEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwHEmpleado.Write(k);
            close();
        }

        public int update(Empleado t)
        {
            open();
            brHEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            long dpos = k * SIZE;
            bwDEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwDEmpleado.Write(++k);
            bwDEmpleado.Write(t.Nombre);
            bwDEmpleado.Write(t.Apellidos);
            bwDEmpleado.Write(t.Cedula);
            bwDEmpleado.Write(t.Inss);
            bwDEmpleado.Write(t.Direccion);
            bwDEmpleado.Write(t.Salario);
            bwDEmpleado.Write(t.Tconvencional);
            bwDEmpleado.Write(t.Tcelular);

            close();
            return t.Id;
        }
    }
}
