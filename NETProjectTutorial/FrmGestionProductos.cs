﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionProductos : Form
    {
        private DataSet dsProductos;
        private BindingSource bsProductos;
        private ProductoModel productoModel;
        public DataSet DsProductos
        {
            get
            {
                return dsProductos;
            }

            set
            {
                dsProductos = value;
            }
        }

        public FrmGestionProductos()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

   
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsProductos.Filter = string.Format("Sku like '*{0}*' or Nombre like '*{0}*' or Descripcion like '*{0}*' ", textBox1.Text);
               
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmProducto fp = new FrmProducto();
            fp.TblProductos = DsProductos.Tables["Producto"];
            fp.DsProductos = DsProductos;
            fp.ShowDialog();
        }

        private void FrmGestionProductos_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = DsProductos;
            bsProductos.DataMember = DsProductos.Tables["Producto"].TableName;
            dataGridView1.DataSource = bsProductos;
            dataGridView1.AutoGenerateColumns = true;

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if(rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow =  rowCollection[0];
            DataRow drow = ((DataRowView) gridRow.DataBoundItem).Row;

            FrmProducto fp = new FrmProducto();
            fp.TblProductos = DsProductos.Tables["Producto"];
            fp.DsProductos = DsProductos;
            fp.DrProducto = drow;
            fp.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result =  MessageBox.Show(this,"Realmente desea eliminar ese registro?","Mensaje del Sistema",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if(result == DialogResult.Yes)
            {
                DsProductos.Tables["Producto"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void FrmGestionProductos_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtProductosAdded = dsProductos.Tables["Cliente"].GetChanges(DataRowState.Added);
            DataTable dtProductosUpdated = dsProductos.Tables["Cliente"].GetChanges(DataRowState.Modified);
            DataTable dtProductosDeleted =dsProductos.Tables["Cliente"].GetChanges(DataRowState.Deleted);

            if (dtProductosAdded != null)
            {
                foreach (DataRow dr in dtProductosAdded.Rows)
                {
                    productoModel.save(dr);
                    dsProductos.Tables["Cliente"].Rows.Find(dr["Id"]).AcceptChanges();
                }

            }

            if (dtProductosUpdated != null)
            {
                foreach (DataRow dr in dtProductosUpdated.Rows)
                {
                    productoModel.update(dr);
                    dsProductos.Tables["Cliente"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }

            if (dtProductosDeleted != null)
            {
                foreach (DataRow dr in dtProductosDeleted.Rows)
                {
                  
                }
            }

        }
    }
}
