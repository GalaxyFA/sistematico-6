﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoProductos : IDao<Producto>
    {
        Producto findbyId(int id);
        Producto findBySKU(string SKU);
        
    }
}
