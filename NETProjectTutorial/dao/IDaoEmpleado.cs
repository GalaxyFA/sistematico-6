﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoEmpleado :IDao<Empleado>
    {
        Empleado findById(int id);
        Empleado findByCedula(string cedula);
        List<Empleado> findByLastname(string lastname);
    }
}
